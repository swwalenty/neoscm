Neoscm - a scaffolding for hackhathon
==================================

This project is and will be maintained purely for needs of Neo4j hackathon. 
This will never be a finished implementation it is just scaffolding of a project to shift focus during hackathon on Neo4j 
and graph modeling.

Purpose of Neoscm hackhathon
============================

The purpose of the hackhathon is to get familiar with Neo4j and graphs modeling through implementation of SCM.

We have defined two goals of this project, one which is must have and second which is nice to have.

*Must have*

* SCM status command which show status of files in workspace
* SCM add command which adds file to be tracked by SCM
* SCM commit command which will commit changes to repository

*Nice to have*

* SCM branch command which will create branch
* SCM checkout command which will checkout revision or branch to workspace

Structure of the project
========================

This is good old (sorry Gradle) maven based project. Just run 'mvn install' and you are done.

We have provided a app assembler which should make work with the project easier. 
Once you install it got to target/appassembler/bin run neoscm script and you are done.

More to come
============

How to create command :)