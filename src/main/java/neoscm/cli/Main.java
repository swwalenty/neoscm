package neoscm.cli;

import neoscm.commands.ScmCommandDispatcher;
import neoscm.commands.ScmCommandOutput;
import neoscm.commands.ScmCommands;
import neoscm.commands.ScmCommandsFinder;

public class Main {

	public static void main(String[] args) {

		ScmCommandsFinder finder = new ScmCommandsFinder();
		ScmCommands scmCommands = finder.searchFor("neoscm.commands");

		ScmCommandDispatcher dispatcher = new ScmCommandDispatcher(scmCommands);

		ScmCommandOutput output = dispatcher.execute(args);

		System.out.println(output.asString());
		
	}

}
