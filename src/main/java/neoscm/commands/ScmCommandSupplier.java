package neoscm.commands;

import java.util.function.Supplier;

/**
 * Look at my name, I supply SCM commands.
 * 
 * @author jaroslaw.palka@symentis.pl
 * @since 1.0.0
 *
 */
public class ScmCommandSupplier implements Supplier<ScmCommand> {

	private final Class<? extends ScmCommand> scmCommandClass;

	public ScmCommandSupplier(Class<? extends ScmCommand> scmCommandClass) {
		super();
		this.scmCommandClass = scmCommandClass;
	}

	@Override
	public ScmCommand get() {
		try {
			return scmCommandClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new IllegalArgumentException("illegal scm command class");
		}
	}

}
