package neoscm.commands;

import static java.util.Arrays.asList;

import java.util.Optional;
import java.util.function.Supplier;

/**
 *
 * Dispatches invocation of neoscm script to proper command.
 *
 * @author jaroslaw.palka@symentis.pl
 * @since 1.0.0
 *
 */
public class ScmCommandDispatcher {

	private final ScmCommands scmCommands;

	public ScmCommandDispatcher(ScmCommands scmCommands) {
		System.out.println(scmCommands.toString());
		this.scmCommands = scmCommands;
	}

	public ScmCommandOutput execute(String[] argv) {

		Optional<String> commandName = asList(argv).stream().findFirst();

		Optional<Supplier<ScmCommand>> command = commandName.flatMap(
                scmCommands::get).map(
                ScmCommandSupplier::new);

		return command.map(c -> c.get().execute(argv)).orElseThrow(
				() -> new IllegalArgumentException("command not found"));

	}
}
