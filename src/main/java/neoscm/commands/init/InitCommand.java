package neoscm.commands.init;

import neoscm.commands.ScmCommand;
import neoscm.commands.ScmCommandOutput;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

@ScmCommand.Description(name = "init")
public class InitCommand implements ScmCommand {

    private static final String CURRENT_DIRECTORY = "user.dir";
    public static final String NEO_DIRECTORY = ".neo";

    @Override
    public ScmCommandOutput execute(String[] argv) {
        String neoDirectory = System.getProperty(CURRENT_DIRECTORY);
        GraphDatabaseService graphDb = new GraphDatabaseFactory()
                .newEmbeddedDatabaseBuilder(neoDirectory + "/" + NEO_DIRECTORY)
                .newGraphDatabase();
        return () -> String.format("Initialized empty NeoScm repository in %s/%s/", neoDirectory, NEO_DIRECTORY);
    }
}
