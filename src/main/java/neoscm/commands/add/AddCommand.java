package neoscm.commands.add;

import java.util.List;

import javassist.NotFoundException;

import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterable;
import org.neo4j.graphdb.Transaction;

import neoscm.commands.ScmCommand;
import neoscm.commands.ScmCommandOutput;
import neoscm.commands.diffs.DBProvider;
import neoscm.commands.diffs.FileLoader;

@ScmCommand.Description(name = "add")
public class AddCommand implements ScmCommand {

	@Override
	public ScmCommandOutput execute(String[] argv) {
		String file = argv[1];
		String changedFile = FileLoader.fileToLines(file);
		Label label = DynamicLabel.label("newCommit");

		GraphDatabaseService instance = DBProvider.getInstance();
		try (Transaction tx = instance.beginTx();) {
			ResourceIterable<Node> findNodesByLabelAndProperty = instance
					.findNodesByLabelAndProperty(label, "version", 1);
			Node newNode = null;
			if (findNodesByLabelAndProperty.iterator().hasNext()) {
				newNode = findNodesByLabelAndProperty.iterator().next();
			} else {
				newNode = instance.createNode(label);
			}
			int version = 1;
			try{
				version = Integer.parseInt(newNode.getProperty("version").toString());
			} catch (org.neo4j.graphdb.NotFoundException e){
				version =1;
			}
			newNode.setProperty(file, changedFile);
			newNode.setProperty(
					"version", version
					);
			tx.success();
		}

		return () -> String.format("Added new file: %s/", file);
	}

}
