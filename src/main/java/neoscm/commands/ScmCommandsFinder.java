package neoscm.commands;

import static java.util.stream.Collectors.toMap;

import java.util.Map;
import java.util.Set;

import neoscm.commands.ScmCommand.Description;

import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;

/**
 * Look at my name, I know how to find SCM commands in classloader.
 * 
 * @author jaroslaw.palka@symentis.pl
 * @since 1.0.0
 *
 */
public class ScmCommandsFinder {

	public ScmCommands searchFor(String packageName) {

		Reflections reflections = new Reflections(new SubTypesScanner(),
				packageName);

		Set<Class<? extends ScmCommand>> subTypesOfScmCommands = reflections
				.getSubTypesOf(ScmCommand.class);

		Map<Description, Class<? extends ScmCommand>> mapOfCommands = subTypesOfScmCommands
				.stream().collect(
						toMap(clazz -> clazz
								.getAnnotation(ScmCommand.Description.class),
								clazz -> clazz));

		return new ScmCommands(mapOfCommands);
	}

}
