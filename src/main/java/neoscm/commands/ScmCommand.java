package neoscm.commands;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * SCM command interface, yes!
 * 
 * @author jaroslaw.palka@symentis.pl
 * @since 1.0.0
 *
 */
public interface ScmCommand {

	/**
	 * 
	 * Meta data about scm command, at the moment it is only command name
	 * 
	 * @author jaroslaw.palka@symentis.pl
	 * @since 1.0.0
	 *
	 */
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.TYPE)
	public @interface Description {
		String name();
	}

	ScmCommandOutput execute(String[] argv);

}
