package neoscm.commands;

import java.util.Map;
import java.util.Optional;

import neoscm.commands.ScmCommand.Description;

/**
 * I am not so trivial registry of SCM commands.
 * 
 * @author jaroslaw.palka@symentis.pl
 * @since 1.0.0
 *
 */
public class ScmCommands {

	private final Map<Description, Class<? extends ScmCommand>> mapOfCommands;

	public ScmCommands(
			Map<Description, Class<? extends ScmCommand>> mapOfCommands) {
		this.mapOfCommands = mapOfCommands;
	}

	public Optional<Class<? extends ScmCommand>> get(String commandName) {
		// @formatter:off
		return mapOfCommands.keySet().stream()
				.filter(
						desc -> commandName.equals(
								desc.name()
						)).findFirst()
				.map(mapOfCommands::get);
		// @formatter:on
	}

}
