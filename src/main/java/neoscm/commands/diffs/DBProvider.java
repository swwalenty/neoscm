package neoscm.commands.diffs;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

public class DBProvider {

    private static final String CURRENT_DIRECTORY = "user.dir";
    public static final String NEO_DIRECTORY = ".neo";
	private GraphDatabaseService graphDb;
	private static DBProvider dbprovider;
	
	
	private DBProvider() {
        String neoDirectory = System.getProperty(CURRENT_DIRECTORY);
		graphDb = new GraphDatabaseFactory()
	    .newEmbeddedDatabase(neoDirectory + "/" + NEO_DIRECTORY);
	}
	
	public static GraphDatabaseService getInstance(){
		if(dbprovider == null ){
			dbprovider = new DBProvider();
		}
		return dbprovider.graphDb;
	}
	

}
