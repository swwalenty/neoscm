package neoscm.commands.diffs;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class FileLoader {
	public static String fileToLines(String filename) {
		StringBuffer lines = new StringBuffer();
		String line = "";
		try {
			BufferedReader in = new BufferedReader(new FileReader(filename));
			while ((line = in.readLine()) != null) {
				lines.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return lines.toString();
	}
	public static List<String> fileFromRepo(String filename){
		return null;
		
	}
}
