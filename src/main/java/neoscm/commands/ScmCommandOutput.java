package neoscm.commands;

/**
 * ScmCommands return me, I am container of output from command.
 * 
 * @author jaroslaw.palka@symentis.pl
 * @since 1.0.0
 *
 */
public interface ScmCommandOutput {

	String asString();

}
