package neoscm.commands;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class ScmCommandDispatcherTest {

	@Test
	public void should_dispatch_to_scm_command() {

		ScmCommandsFinder finder = new ScmCommandsFinder();
		ScmCommands scmCommands = finder.searchFor("neoscm.commands.mocks");

		ScmCommandDispatcher dispatcher = new ScmCommandDispatcher(scmCommands);

		String commandName = "mockcommand";

		String[] argv = new String[] { commandName };

		ScmCommandOutput output = dispatcher.execute(argv);

		assertThat(output.asString()).isEqualTo("you just called mockcommand");

	}

}
