package neoscm.commands;

import static org.junit.Assert.*;
import neoscm.commands.diffs.DBProvider;

import org.junit.Test;
import org.neo4j.graphdb.GraphDatabaseService;

public class DBProviderTest {

	@Test
	public void shouldReturnNotNullGraphDBHandle() {

		//when
		GraphDatabaseService instance = DBProvider.getInstance();
		//then
		assertNotNull(instance);
		
	}

}
