package neoscm.commands;

import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class ScmCommandsFinderTest {

	@Test
	public void should_find_scm_commands() {
		ScmCommandsFinder finder = new ScmCommandsFinder();

		ScmCommands commands = finder.searchFor("neoscm.commands.mocks");

		Optional<Class<? extends ScmCommand>> classOfScmCommand = commands.get("mockcommand");
		Assertions.assertThat(classOfScmCommand.isPresent()).isTrue();
	}

}
