package neoscm.commands.mocks;

import neoscm.commands.ScmCommand;
import neoscm.commands.ScmCommandOutput;

@ScmCommand.Description(name = "mockcommand")
public class MockScmCommand implements ScmCommand {

	@Override
	public ScmCommandOutput execute(String[] argv) {
		return () -> "you just called mockcommand";
	}

}
